class MessageMailer < ApplicationMailer

  default :to => "sdhairstylist@gmail.com"
  def message_me(msg)
    @msg = msg

    mail from: @msg.email, subject: "(Subject: #{@msg.subject}) (email: #{@msg.email}) - (name:#{@msg.name})", body: @msg.content
  end

end
