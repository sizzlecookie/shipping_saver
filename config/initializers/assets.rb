# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w(

  jquery.tmpl.min.js
  gallery.js
  jquery.elastislide.js
  jquery.easing.1.3.js
  elastislide.scss
  style.scss
  demo.scss
  reset.scss
  bootstrap.min.js
  google_analytics.js.coffee
  mdb.min.js
  bootstrap.min.scss
  mdb.min.scss
  style_landing.scss
  )
