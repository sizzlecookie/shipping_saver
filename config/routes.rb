Rails.application.routes.draw do

  resources :messages, only: [:new, :create]

  root to: 'visitors#index'

  devise_scope :user do
    # get "/login" => "devise/sessions#new"
    get "/logout" => "devise/sessions#destroy"
    delete "/logout" => "devise/sessions#destroy"
    # get "/signup" => "devise/registrations#new"
    # get "/settings" => "devise/registrations#edit"
  end

  devise_for :users
  resources :users

  # get 'about' => 'pages#about'
  # get 'blog' => 'pages#blog'
  # get 'contact' => 'pages#contact'

  Rails.application.routes.draw do
  get 'messages/new'

    # get "/:page" => "pages#show"
  end

end
