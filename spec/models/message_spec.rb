require 'rails_helper'

describe Message do
    before { @message = Message.new(name: "Example User", email: "user@example.com", subject: "test123", content: "test test") }
    subject { @message }

    it { should respond_to(:name) }
    it { should respond_to(:email) }
    it { should respond_to(:subject) }
    it { should respond_to(:content) }

    it { should be_valid }

    describe "when name is not present" do
      before { @message.name = " " }
      it { should_not be_valid }
    end

    describe "when email is not present" do
      before { @message.email = " " }
      it { should_not be_valid }
    end

    describe "when subject is not present" do
      before { @message.subject = " " }
      it { should_not be_valid }
    end

    describe "when content is not present" do
      before { @message.content = " " }
      it { should_not be_valid }
    end

    describe "when email format is invalid" do
      it "should be invalid" do
        # TODO: FINISH THIS...
        # pending ("WIP") do
        #   addresses = %w[user@foo,com user_at_foo.org example.user@foo.
        #            foo@bar_baz.com foo@bar+baz.com]
        #   addresses.each do |invalid_address|
        #     @message.email = invalid_address
        #     expect(@message).not_to be_valid
        #   end
        # end
      end
    end
end
